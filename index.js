const checkbox = document.getElementById("hamburger")
const display = document.getElementById("ul-mobile")
const button = document.getElementById("hamburger-btn")
const close = document.getElementById("close-btn")


checkbox.addEventListener("click", () => {
    display.classList.toggle("none")
    button.classList.toggle("none")
    close.classList.toggle("none")
})